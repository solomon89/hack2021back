from flask import Flask
from flask_restful import  abort, Api
from ApiData import Route
from flask_cors import CORS, cross_origin



app = Flask(__name__)
api = Api(app)
app.config['JSON_AS_ASCII'] = False
cors = CORS(app, resources={r"*": {"origins": "*"}})
app.config['CORS_HEADERS'] = 'Content-Type'
app.config['CORS_HEADERS'] = 'Access-Control-Allow-Origin = *'


api.add_resource(Route.RouteList, '/')
api.add_resource(Route.coordinateRoad, '/route/<route_id>')
api.add_resource(Route.Route, '/route/<route_id>')

api.add_resource(Route.HeatMap, '/HeatMap/<date>')
api.add_resource(Route.Navigate, '/Navigate')

if __name__ == '__main__':
    app.run(debug=True)