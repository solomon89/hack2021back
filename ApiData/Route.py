from ApiData.core.getexampleroad import getRoad
from flask import jsonify, request
from flask_restful import reqparse, Resource
import requests
import ApiData.core.heatmap
from ApiData.core.postgresexecutor import execute
import datetime
import json

ROUTES = {
    '1': {'id': 1, 'pointX': '55.697097362829425, ', 'pointY': '37.62176467596452', "place": "Варшавское шоссе 18"},
    '2': {'id': 2, 'pointX': '55.69217631649213', 'pointY': '37.620904656379324', "place": "Варшавское шоссе 19"},
    '3': {'id': 3, 'pointX': '55.66211541004913', 'pointY': '37.61900239870691', "place": "Варшавское шоссе 20"},
    '4': {'id': 4, 'pointX': '55.57552445123564', 'pointY': '37.6009715478916', "place": "Варшавское шоссе 21"},
}
apiKey = 'a8edc68b-62d0-4cbc-aa31-f4e6ac24b160'
maxBufferRadiusForVehicles = {"foot": 500, "bike": 1000, "scooter": 1500}


class Route(Resource):
    def get(self, route_id):
        print(route_id)
        return jsonify(ROUTES[route_id])

    def delete(self, route_id):
        del ROUTES[route_id]
        return '', 204

    def put(self, route_id):
        args = parser.parse_args()
        point = {'point': args['point'], "place": args['place']}
        ROUTES[route_id] = point
        return point, 201


class Navigate(Resource):

    def convertCoord(self, point):
        sql = "SELECT st_astext(ST_Transform('SRID=3857;POINT(" + str(point[0]) + " " + str(point[1])+ ")'::geometry,4326))," \
                                                                                              "ST_X(ST_Transform('SRID=3857;POINT(" + \
              str(point[0]) + " " + str(point[1]) + ")'::geometry,4326))," \
                                          "ST_Y(ST_Transform('SRID=3857;POINT(" + str(point[0]) + " " + str(point[
                  1])+ ")'::geometry,4326))"
        res = execute(sql)
        _return = {'point': res[0][0], 'coord': [res[0][1], res[0][2]]}
        return _return

    def post(self):
        json_data = request.get_json(force=True)
        points=json_data['points']
        vehicle = json_data['vehicle']
        pointRequest = {'points': [self.convertCoord(points[0])['coord'],
                                   self.convertCoord(points[1])['coord']
                                   ],
                        "vehicle": vehicle,
                        "points_encoded": False,
                        "locale": "ru"}
        response = requests.post('https://graphhopper.com/api/1/route?key=' + apiKey, json=pointRequest)
        responseJSON = response.json()
        points = json_data['points']
        _return = []
        try:
            shortWay = 'LINESTRING('
            firstStep = True
            for line in responseJSON['paths'][0]['points']['coordinates']:
                if firstStep:
                    shortWay = shortWay + str(line[0]) + ' ' + str(line[1])
                    firstStep = False
                else:
                    shortWay = shortWay + ', ' + str(line[0]) + ' ' + str(line[1])
                hasLine = True
            shortWay = shortWay + ')'
        except:
            hasLine = False
        try:
            vehicleBuffRadius = maxBufferRadiusForVehicles[vehicle]
        except:
            vehicleBuffRadius = 500
        startPoint = self.convertCoord(points[0])['coord']
        endPoint = self.convertCoord(points[1])['coord']
        timeInWayStr = responseJSON['paths'][0]['time']
        distance = responseJSON['paths'][0]['distance']
        timeInWaySec = float(timeInWayStr) / 1000
        if hasLine:
            coordinates = [responseJSON['paths'][0]['points']['coordinates']]
            route = {'type': 'MultiLineString', 'coordinates': coordinates,
                     "routeType": "shortest", "distance": distance, 'time': timeInWaySec}
            _return.append(route)
            sql = ' INSERT INTO public."Routes"("startPoint", "endPoint", "shortWay", "distance", "wayTime")' \
                  " VALUES (ST_GeomFromText('" + self.convertCoord(startPoint)['point'] + "',4326)," \
                          " ST_GeomFromText('" + self.convertCoord(endPoint)['point'] + "',4326)," \
                            " ST_GeomFromText('" + shortWay + "',4326), " + str(
                timeInWaySec) + "," + str(distance) + ")" \
                                                      ' RETURNING "ID"'
        else:
            sql = ' INSERT INTO public."Routes"("startPoint", "endPoint")' \
                  " VALUES (ST_GeomFromText('" + self.convertCoord(startPoint)['point'] + "',4326)," \
                          " ST_GeomFromText('" + self.convertCoord(endPoint)['point'] + "',4326))" \
                            ' RETURNING "ID"'
        DBResult = execute(sql)
        routeID = int(DBResult[0][0])
        dateTimeNow = datetime.datetime.now() - datetime.timedelta(hours=1)
        timeNow = dateTimeNow.strftime("%H:%M:%S")
        datetimeThen = dateTimeNow + datetime.timedelta(hours=1, seconds=timeInWaySec)
        timeThen = datetimeThen.strftime("%H:%M:%S")
        ##Строим буфферные зоны чистой зоны
        sql = '''INSERT INTO public."RouteCalcBuffer"(
                 "BufferZone", "CalcID", "AQI", "type")
                SELECT ST_Transform(ST_Buffer(ST_Transform(ep.geoposition, _ST_BestSRID(ep.geoposition)),
                 case when ((100-rdeavg.aqi)*10)>''' + str(vehicleBuffRadius) + ''' then ''' + str(vehicleBuffRadius) + ''' else ((100-rdeavg.aqi)*10) end, 'quad_segs=8 endcap=round'),4326)  as buffer,
                  ''' + str(routeID) + ''' as routeId,
                  rdeavg.aqi, 1
                from public.ecopoints ep
                join (SELECT rde.point_id, avg(rde.aqi) as aqi 
                      from public.rowdataofecology rde ''' \
                                       " where cast(rde.dataofevent as time)>=cast('" + timeNow + "' as time)" \
                                                                                                  "  and cast(rde.dataofevent as time)<=cast('" + timeThen + "' as time)" \
                                                                                                                                                             '''      group by point_id) rdeavg on ep.id=rdeavg.point_Id 
                                                                                                                                                             where rdeavg.aqi<=100'''
        execute(sql)
        ##Строим буфферные зоны грязной зоны
        sql = '''INSERT INTO public."RouteCalcBuffer"(
                         "BufferZone", "CalcID", "AQI", "type")
                        SELECT ST_Transform(ST_Buffer(ST_Transform(ep.geoposition, _ST_BestSRID(ep.geoposition)),
                         case when ((rdeavg.aqi))>''' + str(vehicleBuffRadius) + ''' then ''' + str(
            vehicleBuffRadius) + ''' else ((rdeavg.aqi)) end, 'quad_segs=8 endcap=round'),4326)  as buffer,
                          ''' + str(routeID) + ''' as routeId,
                          rdeavg.aqi, 0
                        from public.ecopoints ep
                        join (SELECT rde.point_id, avg(rde.aqi) as aqi 
                              from public.rowdataofecology rde ''' \
                                               " where cast(rde.dataofevent as time)>=cast('" + timeNow + "' as time)" \
                                                                                                          "  and cast(rde.dataofevent as time)<=cast('" + timeThen + "' as time)" \
                                                                                                                                                                     '''      group by point_id) rdeavg on ep.id=rdeavg.point_Id 
                                                                                                                                                                     where rdeavg.aqi>100'''
        execute(sql)
        sql = '''INSERT INTO public."GreenRoutePoints"(
                "greenPoint", "shortRouteId", "distanceToObject")
                SELECT ST_Centroid(prks.geom::geography)::geometry, rts."ID",
                ST_Distance(ST_ClosestPoint(prks.geom, rts."shortWay")::geography,ST_ClosestPoint(rts."shortWay",prks.geom)::geography)     
                FROM public."Routes" rts, public."Parks" prks
                where ST_Distance(ST_ClosestPoint(prks.geom, rts."shortWay")::geography,ST_ClosestPoint(rts."shortWay",prks.geom)::geography)<=''' + str(
            vehicleBuffRadius) + '' \
                                 ' AND rts."ID"= ' + str(routeID)

        execute(sql)

        sql = '''select ST_X("greenPoint"),
                        ST_Y("greenPoint")
                from public."GreenRoutePoints"
                WHERE "shortRouteId"=''' + str(routeID)

        greenPoints = execute(sql)
        coordinates = [responseJSON['paths'][0]['points']['coordinates']]
        for point in greenPoints:
            pointRequest = {'points': [[startPoint[0], startPoint[1]],
                                       [point[0], point[1]],
                                       [endPoint[0], endPoint[1]]
                                       ],
                            "vehicle": vehicle,
                            "points_encoded": False,
                            "locale": "ru"}

            response = requests.post('https://graphhopper.com/api/1/route?key=' + apiKey, json=pointRequest)
            responseJSON = response.json()
            try:
                greenWay = 'LINESTRING('
                firstStep = True
                for line in responseJSON['paths'][0]['points']['coordinates']:
                    if firstStep:
                        greenWay = greenWay + str(line[0]) + ' ' + str(line[1])
                        firstStep = False
                    else:
                        greenWay = greenWay + ', ' + str(line[0]) + ' ' + str(line[1])
                    hasLine = True
                greenWay = greenWay + ')'
            except:
                hasLine = False
            if hasLine:
                timeInWayStr = responseJSON['paths'][0]['time']
                distance = responseJSON['paths'][0]['distance']
                timeInWaySec = float(timeInWayStr) / 1000
                sql = '  INSERT INTO public."GreenRoutes"(' \
                      '             "shortRoutId", "GreenRoute","distance","wayTime")' \
                      "  VALUES(" + str(routeID) + ",ST_GeomFromText('" + greenWay + "',4326), " + str(
                    timeInWaySec) + "," + str(distance) + ")"
                execute(sql)
                coordinates = [responseJSON['paths'][0]['points']['coordinates']]
                route = {'type': 'MultiLineString', 'coordinates': coordinates,
                         "routeType": "green", "distance": distance, 'time': timeInWaySec}
                _return.append(route)

        return jsonify(_return)


parser = reqparse.RequestParser()
parser.add_argument('point')
parser.add_argument('place')


class coordinateRoad(Resource):
    def get(self, route_id):
        road = getRoad(route_id)
        return jsonify(road)


class RouteList(Resource):
    def get(self):
        return jsonify(ROUTES)

    def post(self):
        args = parser.parse_args()
        point_id = int(max(ROUTES.keys()).lstrip('point')) + 1
        point_id = 'point%i' % point_id
        ROUTES[point_id] = {'point': args['point'], "place": args['place']}
        return ROUTES[point_id], 201


class HeatMap(Resource):
    def get(self):
        return 200
