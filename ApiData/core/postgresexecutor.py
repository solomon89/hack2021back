import psycopg2
from psycopg2 import Error
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
def execute(sqlExpression):
    try:
        # Подключение к существующей базе данных
        connection = psycopg2.connect(user="postgres",
                                    # пароль, который указали при установке PostgreSQL
                                    password="1",
                                    host="stavteam2021.ddns.net",
                                    port="5433",
                                    database="hackdb")
        connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        # Курсор для выполнения операций с базой данных
        cursor = connection.cursor()
        cursor.execute(sqlExpression)
        try:
            data = cursor.fetchall()
        except:
            data = None
        dataFromDB = []
        if (data != None):
            dataFromDB = data
    except (Exception, Error) as error:
        dataFromDB = None
        print("Ошибка при работе с PostgreSQL", error)
    finally:
        if connection:
            cursor.close()
            connection.close()
            print("Соединение с PostgreSQL закрыто")
            return dataFromDB