import postgresexecutor
import json

Sql = """-- generate series 
select ST_AsGeoJSON(ST_Centroid(TableLines.geom, True)),  
       ST_AsGeoJSON(TableLines.geom), 
       ST_AsGeoJSON(ST_PointOnSurface(TableLines.geom) )
from  
(WITH RECURSIVE generate_sections(id, sec) AS ( 
SELECT conf.start + 1,  
    conf.start 
FROM conf 
UNION ALL 
SELECT id + conf.step, sec + conf.step 
FROM generate_sections, conf 
WHERE sec + conf.step < conf.sections 
), 
 
-- configurations 
conf AS ( 
SELECT 5 AS sections, 
       0.0 AS start, 
      1.0 AS step 
) 
 
-- query 
SELECT gs.id AS id, 
    ST_LineSubstring(l.geom, conf.start + sec/conf.sections, sec/conf.sections + step/conf.sections) AS geom, 
    round(cast(ST_Length(ST_LineSubstring(l.geom, conf.start + sec/conf.sections, sec/conf.sections + step/conf.sections))as numeric(15,4)),2) AS seg_length 
FROM generate_sections AS gs, public."Объединенный слой" AS l, conf 
WHERE start + step < sections 
GROUP BY gs.id,l.geom,conf.start,gs.sec,conf.sections,conf.step) as TableLines"""
data = postgresexecutor.execute(Sql)
FinalRoad = []

for row in data:
    roadObject = json.loads(row[1])
    FinalRoad.extend(roadObject['coordinates'][0])
print(FinalRoad)    